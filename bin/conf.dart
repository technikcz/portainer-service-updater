import 'package:conf/conf.dart';

class Config {
  factory Config._factory(Map<String, Object?> map) => Config(
        host: map['host']! as String,
        apiKey: map['api-key']! as String,
        dockerVersion: map['docker-version']! as String,
        endpointId: map['endpoint-id']! as String,
        stackId: map['stack-id']! as String,
        pullImages: map['pull-images']! as bool,
        pruneImages: map['prune-images']! as bool,
        stackVariableName: map['stack-variable-name']! as String,
      );

  static final schema = ConfObject(
    propertiesMap: {
      'host': ConfString(),
      'api-key': ConfString(),
      'docker-version': ConfDefault(ConfString(), defaultValue: 'latest'),
      'endpoint-id': ConfString(),
      'stack-id': ConfString(),
      'pull-images': ConfDefault(ConfBoolean(), defaultValue: false),
      'prune-images': ConfDefault(ConfBoolean(), defaultValue: false),
      'custom-headers': ConfList(ConfString()),
      'stack-variable-name': ConfDefault(ConfString(), defaultValue: 'SERVICE_VERSION')
    },
    factory: Config._factory,
  );

  final String host;
  final String apiKey;
  final String dockerVersion;
  final String endpointId;
  final String stackId;
  final bool pullImages;
  final bool pruneImages;
  final String stackVariableName;

  Config({
    required this.host,
    required this.apiKey,
    required this.dockerVersion,
    required this.endpointId,
    required this.stackId,
    required this.pullImages,
    required this.pruneImages,
    required this.stackVariableName,
  });
}
