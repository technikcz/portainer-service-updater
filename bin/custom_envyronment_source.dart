import 'dart:io';

import 'package:conf/conf.dart';

class CustomEnvironmentSource implements ConfigurationSource {
  final Map<String, String> _environment;

  CustomEnvironmentSource(Map<String, String> environment)
      : _environment = (_normalizeEnvironment(environment));

  factory CustomEnvironmentSource.fromEnvironment() =>
      CustomEnvironmentSource(Platform.environment);

  static Map<String, String> _normalizeEnvironment(
      Map<String, String> environment,
      ) =>
      environment.map((key, value) => MapEntry(key.toUpperCase(), value));

  static String _environmentVariableNameForKey(ConfigurationKey key) {
    final stringBuilder = StringBuffer();
    var isFirst = true;
    for (final segment in key.path) {
      if (segment is String) {
        if (!isFirst) {
          stringBuilder.write('_');
        }
        stringBuilder.write(segment.toUpperCase().replaceAll("-", "_"));
      } else {
        stringBuilder
          ..write('_')
          ..write(segment);
      }
      isFirst = false;
    }
    return stringBuilder.toString();
  }

  @override
  String get description => 'environment variables';

  @override
  String? operator [](ConfigurationKey key) =>
      _environment[_environmentVariableNameForKey(key)];

  @override
  bool contains(ConfigurationKey key) {
    final prefix = _environmentVariableNameForKey(key);
    return _environment.keys.any(
          (key) =>
      key.startsWith(prefix) &&
          // Prefix must end at a segment boundary.
          (key.length == prefix.length || key[prefix.length] == '_'),
    );
  }

  @override
  String describeKey(ConfigurationKey key) =>
      _environmentVariableNameForKey(key);


}
