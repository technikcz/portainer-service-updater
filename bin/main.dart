import 'dart:convert';
import 'dart:io';

import 'package:conf/conf.dart';
import 'package:dio/dio.dart';

import 'conf.dart';
import 'custom_envyronment_source.dart';

void main(List<String> arguments) async {
  final source = CombiningSource([
    CustomEnvironmentSource(Platform.environment),
    CommandLineSource(arguments),
  ]);

  try {
    final config = await Config.schema.load(source);
    final customHeaders = parseCustomHeaders();
    final client = Dio();

    final headers = <String, dynamic>{
      'Accept': 'application/json',
      'ContentType': 'application/json',
      'X-API-KEY': config.apiKey,
      if (customHeaders.isNotEmpty) ...customHeaders,
    };

    final stackComposeConfigResponse = await client.get<String>(
      '${config.host}/api/stacks/${config.stackId}/file',
      options: Options(
        validateStatus: (s) => true,
        headers: headers,
      ),
    );

    if ((stackComposeConfigResponse.statusCode ?? 400) >= 400) {
      print(stackComposeConfigResponse.data);
      print('');
      exitCode = 1;
      return;
    }

    final stackComposeConfig =
        jsonDecode(stackComposeConfigResponse.data ?? '{}')['StackFileContent'];

    final stackUpdateConfig = {
      'id': config.stackId,
      'env': [
        {
          'name': config.stackVariableName,
          'value': config.dockerVersion,
        }
      ],
      'prune': config.pruneImages,
      'pull': config.pullImages,
      'stackFileContent': stackComposeConfig,
    };

    final stackUpdateResponse = await client.put(
      '${config.host}/api/stacks/${config.stackId}',
      queryParameters: {
        'endpointId': config.endpointId,
      },
      options: Options(headers: headers, validateStatus: (s) => true),
      data: stackUpdateConfig,
    );

    if ((stackUpdateResponse.statusCode ?? 400) >= 400) {
      print(stackUpdateResponse.data);
      print('');
      exitCode = 1;
      return;
    }
  } on ConfigurationException catch (error) {
    stderr.writeln(error);
    exitCode = 1;
  }
}

Map<String, String> parseCustomHeaders() {
  final headersFile = File('headers.json');

  if(!headersFile.existsSync()) return const {};

  return Map
      .from(jsonDecode(headersFile.readAsStringSync()))
      .cast<String, String>();
}
